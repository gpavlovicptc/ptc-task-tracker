package com.ptc.task.tracker.api.exception.feign;

/**
 * Response Transformation for "Average Processor" Microservice.
 * 
 * 
 */
public class FeignErrorMessage {
  private String message;

  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }

}
