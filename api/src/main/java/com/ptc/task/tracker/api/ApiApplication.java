package com.ptc.task.tracker.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * Api Application Microservice Main Class.
 * 
 * 
 */

@SpringBootApplication
@ComponentScan
@EnableFeignClients(basePackages = {"com.ptc.task.tracker.api.client"})
public class ApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiApplication.class);
	}

}
