package com.ptc.task.tracker.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.api.resource.dto.TaskDto;
import com.ptc.task.tracker.api.service.event.EventProducerService;
import com.ptc.task.tracker.api.service.event.TaskEvent;

/**
 * Task Service Implementation. Saves the task in a Stream.
 * 
 * 
 */
@Service
public class TaskServiceImpl implements TaskService {

	@Autowired  
	private EventProducerService eventProducer;
	
	@Override
	public void saveTask(TaskDto taskDto) { 
       TaskEvent event = new TaskEvent(taskDto);
       eventProducer.send(event);
	}

}
