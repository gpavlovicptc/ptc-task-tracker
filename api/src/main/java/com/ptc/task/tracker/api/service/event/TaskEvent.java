package com.ptc.task.tracker.api.service.event;

import java.io.Serializable;

import com.ptc.task.tracker.api.resource.dto.TaskDto;

/**
 * Task Event to be sent it to the Stream.
 * 
 * 
 */
public class TaskEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	private static final String KIND = "NEW_TASK";

	private TaskDto task;

	public TaskEvent(TaskDto task) {
		this.task = task;
	}

	public static String getKind() {
		return KIND;
	}

	public TaskDto getTask() {
		return task;
	}
}
