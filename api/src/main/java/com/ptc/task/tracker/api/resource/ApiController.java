package com.ptc.task.tracker.api.resource;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ptc.task.tracker.api.client.AverageClient;
import com.ptc.task.tracker.api.client.AverageDto;
import com.ptc.task.tracker.api.resource.dto.TaskDto;
import com.ptc.task.tracker.api.service.TaskService;

/**
 * Main Controller.
 * 
 * 
 */
@RestController
@RequestMapping(value = "/tasks")
public class ApiController {

	@Autowired
	private TaskService taskService;

	@Autowired 
	private AverageClient averageClient; 
	
	/**
	 * Saves a Task. Returns 200 is Ok or
	 * 400 if there is some errors(by Exception).
	 * 
	 * @param taskDto
	 */
	@RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public void saveTask(@Valid @RequestBody TaskDto taskDto) {
		taskService.saveTask(taskDto);
	}

	/**
	 * Returns 200 and the Average information or
	 * 404 (by Exception).
	 * 
	 * @param taskName the task name
	 * @return the average information
	 */
	@RequestMapping(value = "/{taskName}/average", method = RequestMethod.GET, 
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public AverageDto getAverage(@PathVariable("taskName") String taskName){
		return averageClient.findAverage(taskName);
	}
		
}
