package com.ptc.task.tracker.api.service;

import com.ptc.task.tracker.api.resource.dto.TaskDto;

/**
 * Task Service Interface.
 * 
 * 
 *
 */
public interface TaskService {

	/**
	 * Saves a task.
	 * 
	 * @param taskDto
	 */
	void saveTask(TaskDto taskDto);
	
}
