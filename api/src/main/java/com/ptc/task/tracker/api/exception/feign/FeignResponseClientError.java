package com.ptc.task.tracker.api.exception.feign;

import java.util.Set;

/**
 * Response Transformation for "Average Processor" Microservice.
 * It is useful for convert it to nice response. 
 * 
 * 
 */
public class FeignResponseClientError {

	private int status;

	private Set<String> messages;

	private String uuid;

	private String resourceService;

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getUuid() {
		return uuid;
	}

	public void setUuid(String uuid) {
		this.uuid = uuid;
	}

	public String getResourceService() {
		return resourceService;
	}

	public void setResourceService(String resourceService) {
		this.resourceService = resourceService;
	}

	public Set<String> getMessages() {
		return messages;
	}

	public void setMessages(Set<String> messages) {
		this.messages = messages;
	}
}
