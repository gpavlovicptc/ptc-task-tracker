package com.ptc.task.tracker.api.config;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ptc.task.tracker.api.exception.ResponseApiError;
import com.ptc.task.tracker.api.exception.feign.FeignClientException;

/**
 * ApiExceptionHandler handles the errors in Api Microservice.
 * 
 * 
 */
@ControllerAdvice
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(ApiExceptionHandler.class);

	private static final String ERROR_UUID_SUFFIX = " - Error uuid: ";

	/**
	 * Handle the Default Exceptions.
	 * 
	 * @param ex the exception thrown
	 * @param the current request
	 * @return the custom response
	 */
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleDefaultExceptions(final Exception ex, final WebRequest request) {
		Set<String> errorMessages = new HashSet<>();
		errorMessages.add(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());

		return getExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, errorMessages, ex);
	}

	/**
	 * Handle the Feign Exceptions.
	 * 
	 * @param ex the exception thrown
	 * @param the current request
	 * @return the custom response
	 */
	@ExceptionHandler({ FeignClientException.class })
	public ResponseEntity<Object> handleFeignClientException(final FeignClientException ex, final WebRequest request) {

		Set<String> errorMessages = new HashSet<>();

		for (String errorMessage : ex.getResponseClientError().getMessages()) {

			errorMessages.add(errorMessage);
		}

		return getExceptionResponse(HttpStatus.valueOf(ex.getStatus()), errorMessages, ex);

	}

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		Set<String> errors = new HashSet<String>();

		for (FieldError error : ex.getBindingResult().getFieldErrors()) {
			errors.add(error.getField() + ": " + error.getDefaultMessage());
		}

		for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
			errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
		}

		ResponseApiError apiError = ResponseApiError.builder().status(HttpStatus.BAD_REQUEST.value()).message(errors)
				.build();

		return handleExceptionInternal(ex, apiError, headers, HttpStatus.BAD_REQUEST, request);
	}

	/**
	 * Build the Custom Error Response.
	 * 
	 * @param statusCode the{@link HttpStatus} to be throw
	 * @param messages the messages to show
	 * @param ex the exception 
	 * @return custom REST response.
	 */
	private ResponseEntity<Object> getExceptionResponse(HttpStatus statusCode, Set<String> messages, Exception ex) {
	
		ResponseApiError apiError;

		if (HttpStatus.INTERNAL_SERVER_ERROR.equals(statusCode)) {
			String uuid = UUID.randomUUID().toString();
			LOG.error(ERROR_UUID_SUFFIX + uuid, ex);
			apiError = ResponseApiError.builder().status(statusCode.value()).message(messages).uuid(uuid).build();
		} else {
			apiError = ResponseApiError.builder().status(statusCode.value()).message(messages).build();
		}

		return new ResponseEntity<>(apiError, new HttpHeaders(), statusCode);
	}
}
