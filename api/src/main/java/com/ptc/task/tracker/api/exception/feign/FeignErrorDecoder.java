package com.ptc.task.tracker.api.exception.feign;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.ResponseEntityDecoder;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;

import java.io.IOException;

import feign.FeignException;
import feign.Response;
import feign.codec.DecodeException;
import feign.codec.ErrorDecoder;

/**
 * As API Microservice is using feign as a rest client,
 * we need to set a custom error decoder in order to 
 * identify / convert the error messager from "Average-Process" 
 * Microservice.
 * 
 * 
 */
public class FeignErrorDecoder implements ErrorDecoder {

 private ErrorDecoder delegate = new ErrorDecoder.Default();

 private final ResponseEntityDecoder decoder;

 public FeignErrorDecoder(ObjectFactory<HttpMessageConverters> messageConverters) {
   this.decoder = new ResponseEntityDecoder(new SpringDecoder(messageConverters));
 }

 @Override
 public Exception decode(String methodKey, Response response) {

   try {

     FeignResponseClientError responseApiError =
         (FeignResponseClientError) decoder.decode(response, FeignResponseClientError.class);
     throw new FeignClientException(response.status(), responseApiError);

   } catch (DecodeException e) {
     e.printStackTrace();
   } catch (FeignException e) {
     e.printStackTrace();
   } catch (IOException e) {
     e.printStackTrace();
   }

   return delegate.decode(methodKey, response);
 }

}