package com.ptc.task.tracker.api.service.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.api.config.EventsSourceConfig.EventsSource;

/**
 * Event Producer Implementation.
 * 
 * 
 */
@Service
public class EventProducerServiceImpl implements EventProducerService {

	@Autowired
	private EventsSource source;
	
	@Override
	public void send(TaskEvent event) {
		source.taskEvents().send(new GenericMessage<>(event));
	}

}
