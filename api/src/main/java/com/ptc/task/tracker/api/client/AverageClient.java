package com.ptc.task.tracker.api.client;

/**
 * Average Client Interface.
 * 
 * 
 */
public interface AverageClient {

	/**
	 * Find Average by task name.
	 * 
	 * @param taskName the task name.
	 * @return a Average Dto
	 */
	AverageDto findAverage(String taskName);
}
