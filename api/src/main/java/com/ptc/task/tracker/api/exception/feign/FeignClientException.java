package com.ptc.task.tracker.api.exception.feign;

/**
 * Response Transformation for "Average Processor" Microservice.
 * 
 * 
 */
public class FeignClientException extends RuntimeException {

  private static final long serialVersionUID = 1L;
  
  private int status;

  private FeignResponseClientError responseClientError;
  
  public FeignClientException(int status, FeignResponseClientError responseClientError){
    super();
    this.status = status;
    this.responseClientError = responseClientError;
  }

  public int getStatus() {
    return status;
  }

  public FeignResponseClientError getResponseClientError() {
    return responseClientError;
  }
  
}
