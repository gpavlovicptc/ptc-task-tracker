package com.ptc.task.tracker.api.resource.dto;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Task Dto. The task information used in
 * the main controller and services.
 * 
 * 
 */
public class TaskDto {

	@NotNull
	@Size(min=1)
	private String taskName;

	@NotNull
	@Min(1)
    //1 Week Max
	@Max(604800000)
	private Long taskDuration;

	public TaskDto(){
	}

	public TaskDto(String taskName, Long taskDuration){
		this.taskName = taskName;
		this.taskDuration = taskDuration;
	}
	
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getTaskDuration() {
		return taskDuration;
	}

	public void setTaskDuration(Long taskDuration) {
		this.taskDuration = taskDuration;
	}

}
