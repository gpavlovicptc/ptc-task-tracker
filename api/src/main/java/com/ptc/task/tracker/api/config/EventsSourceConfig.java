package com.ptc.task.tracker.api.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;

/**
 * Config the Stream where to push the events.
 * 
 * 
 */
@Configuration
@EnableBinding(EventsSourceConfig.EventsSource.class)
public class EventsSourceConfig {
	public interface EventsSource {

		String TASK_EVENTS = "task-events-source";

		@Output(TASK_EVENTS)
		MessageChannel taskEvents();

	}
}
