package com.ptc.task.tracker.api.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Feign Rest Client.
 * 
 * 
 */
@FeignClient("average-processor")
public interface AverageFeignClient {
	
	/**
	 * Get average from external resource.
	 * 
	 * @param taskName the task name.
	 * @return {@link AverageDto} with the task information.
	 */
	@RequestMapping(value = "/averages/{taskName}", method = RequestMethod.GET, 
			consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public AverageDto getAverage(@PathVariable("taskName") String taskName);
}
