package com.ptc.task.tracker.api.service.event;

/**
 * Event Producer Interface.
 * 
 * 
 */
public interface EventProducerService {

	/**
	 * Send a Task Event to the Stream.
	 * 
	 * @param event the Task Event
	 */
	void send(TaskEvent event);
	
}
