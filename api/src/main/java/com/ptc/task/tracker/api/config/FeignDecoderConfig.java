package com.ptc.task.tracker.api.config;

import org.springframework.beans.factory.ObjectFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.ptc.task.tracker.api.exception.feign.FeignErrorDecoder;

/**
 * Instance Feign Error Decoder in order to set a custom 
 * exception for feign client.
 * 
 * 
 */
@Configuration
public class FeignDecoderConfig {
  
  @Autowired
  private ObjectFactory<HttpMessageConverters> messageConverters;
    
  @Bean
  public FeignErrorDecoder apiErrorDecoder() {
    return new FeignErrorDecoder(messageConverters);
  }
  
}
