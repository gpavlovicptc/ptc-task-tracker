package com.ptc.task.tracker.api.exception;

import java.util.Set;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Response API Error.
 * 
 * 
 */
public class ResponseApiError {

	private int status;

	private Set<String> messages;

	@JsonInclude(JsonInclude.Include.NON_NULL)
	private String uuid;

	public int getStatus() {
		return status;
	}

	public Set<String> getMessages() {
		return messages;
	}

	public String getUuid() {
		return uuid;
	}

	public static Builder builder() {
		return new Builder();
	}

	public static class Builder {
		private ResponseApiError apiError;

		private Builder() {
			this.apiError = new ResponseApiError();
		}

		public Builder status(int status) {
			this.apiError.status = status;
			return this;
		}

		public Builder message(Set<String> messages) {
			this.apiError.messages = messages;
			return this;
		}

		public Builder uuid(String uuid) {
			this.apiError.uuid = uuid;
			return this;
		}

		public ResponseApiError build() {
			return apiError;
		}
	}
}
