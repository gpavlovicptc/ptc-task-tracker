package com.ptc.task.tracker.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Average Client Impl.
 * 
 * 
 */
@Service
public class AverageClientImpl implements AverageClient {

	@Autowired
	private AverageFeignClient averageFeignClient;
	
	@Override
	public AverageDto findAverage(String taskName) {
		return averageFeignClient.getAverage(taskName);
	}

}
