package com.ptc.task.tracker.api.client;

/**
 * Average Dto. Represents the information
 * from external resource.
 * 
 * 
 */
public class AverageDto {

	private String taskName;
	
	private Long averageTime;

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getAverageTime() {
		return averageTime;
	}

	public void setAverageTime(Long averageTime) {
		this.averageTime = averageTime;
	}
	
}
