package com.ptc.task.tracker.api.service;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.api.resource.dto.TaskDto;
import com.ptc.task.tracker.api.service.event.EventProducerService;
import com.ptc.task.tracker.api.service.event.TaskEvent;

/**
 * Task Service Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TaskServiceImplTest {

	private static final String TASK_NAME = "task-20";

	private static final Long TASK_MILISECONDS = 60480000L;

	@Mock
	private EventProducerService eventProducerMock;

	@InjectMocks
	private TaskService taskService = new TaskServiceImpl();

	private TaskDto taskDto;

	@Before
	public void setUp() {
		taskDto = new TaskDto(TASK_NAME, TASK_MILISECONDS);
	}

	@Test
	public void saveTaskTest() {

		taskService.saveTask(taskDto);

		verify(eventProducerMock).send(any(TaskEvent.class));

	}

}
