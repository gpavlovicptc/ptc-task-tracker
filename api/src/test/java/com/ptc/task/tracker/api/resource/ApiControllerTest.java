package com.ptc.task.tracker.api.resource;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ptc.task.tracker.api.client.AverageClient;
import com.ptc.task.tracker.api.resource.dto.TaskDto;
import com.ptc.task.tracker.api.service.TaskService;

/**
 * Api Controller Unit Test
 * 
 * 
 */
@RunWith(SpringRunner.class)
@WebMvcTest
public class ApiControllerTest {

	private static final String URL_GET = "/tasks/task-20/average";
	
	private static final String URL_POST = "/tasks"; 
	
	private static final String TASK_NAME = "task-20"; 
	
	private static final Long TASK_MILISECONDS = 60480000L;
	
	private static final Long TASK_MILISECONDS_MAX = 604800001L;
	
	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private TaskService taskService;

	@MockBean
	private AverageClient averageClient;

	@Test
	public void testGetAverageOkInformation() throws Exception {
		this.mockMvc.perform(get(URL_GET)
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());
	}

	@Test
	public void testPostTaskOkInformation() throws Exception {
		TaskDto input = new TaskDto(TASK_NAME, TASK_MILISECONDS);
		this.mockMvc
				.perform(post(URL_POST)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(input)))
						.andExpect(status().isOk());
	}
	
	@Test
	public void testPostTaskMaxMilisecondsAndReturnBadRequest() throws Exception {
		TaskDto input = new TaskDto(TASK_NAME, TASK_MILISECONDS_MAX);
		this.mockMvc
				.perform(post(URL_POST)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(input)))
						.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testPostTaskNullMilisecondsAndReturnBadRequest() throws Exception {
		TaskDto input = new TaskDto(TASK_NAME, null);
		this.mockMvc
				.perform(post(URL_POST)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(input)))
						.andExpect(status().isBadRequest());
	}
	
	@Test
	public void testPostTaskNullNameAndReturnBadRequest() throws Exception {
		TaskDto input = new TaskDto(null, TASK_MILISECONDS);
		this.mockMvc
				.perform(post(URL_POST)
						.contentType(MediaType.APPLICATION_JSON)
						.content(new ObjectMapper().writeValueAsString(input)))
						.andExpect(status().isBadRequest());
	}

}
