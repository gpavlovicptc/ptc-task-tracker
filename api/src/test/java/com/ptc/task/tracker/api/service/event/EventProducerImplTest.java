package com.ptc.task.tracker.api.service.event;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.MessageChannel;

import com.ptc.task.tracker.api.config.EventsSourceConfig.EventsSource;
import com.ptc.task.tracker.api.resource.dto.TaskDto;

/**
 * Event Producer Service Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class EventProducerImplTest {

	private static final String TASK_NAME = "task-20";

	private static final Long TASK_MILISECONDS = 60480000L;

	@Mock
	private EventsSource sourceMock;

	@InjectMocks
	private EventProducerServiceImpl eventProducer;
	
	@Mock
	private TaskEvent taskEvent;

	@Before
	public void setUp() {
		taskEvent = new TaskEvent(new TaskDto(TASK_NAME, TASK_MILISECONDS));
	}

	@Test
	public void sendEventTest() {

		MessageChannel channel = mock(MessageChannel.class);

		when(sourceMock.taskEvents()).thenReturn(channel);

		eventProducer.send(taskEvent);

		verify(sourceMock).taskEvents();

	}

}
