# PTC Task Tracker

## Assumptions
- Build a scalable application
- Time in milliseconds [Long]
- Max value in milliseconds is 604800000 (1 week)
- Task name [String]  
- Average in milliseconds [Long]

## Overview

![Alt text](docs/ptc-overview.png?raw=true "PTC Overview")

This application was developed based on Event Sourcing & CQRS (Command Query Responsibility Segregation) patterns. Is not a pure architecture by it follows the idea.

I chose this approach in order to get a good Scalability & Resiliency.

Event Sourcing ensures that all changes to application state are stored as a sequence of events and CQRS splits the writes & reads operations.  

The application is compound by 3 microservices, but with a easy refactor can be used as a monolithic application.

1. Api: Provides Rest operations to ADD task information or GET the average by task.
When a new POST arrives, the information will be push it to a Stream in order to be processed by store-processor microservice.

2. store-processor: Reads the task events and stores the information to the database. After that, publishes an Average Event to advise that the information was updated. **Note: the store-processor does not save state, only saves the information as a sequence of events.**

3. average-processor: Reads the Average Event, connects to the database and calculates the average using the Database Engine. When the average information is retrieved, the microservice stores it in Redis in order to cache it.
In adition the microservice provide a REST [GET], that Api Microservice use it to get the average by task name.


## Microservices Address

| Microservice  | Url |
| ------------- | ------------- |
| Api | http://localhost:4011 |
| store-processor  | http://localhost:4012|
| average-processor  | http://localhost:4013 |

## Rest documentation
### Api Microservice
- Add New Task information:

      curl -X POST \
      http://localhost:4011/tasks \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/json' \
      -d '{
       "taskName": "task-1",
       "taskDuration": 157
      }'

    - Returns
      - 200: Information received
      - 400: Bad Request
      - 500: Internal Server Error       


- Get Average Time by Task Name:

      curl -i -H 'Accept: application/json' \
      -H 'Content-Type: application/json' \
      -X GET http://localhost:4011/tasks/task-1/average

  - Returns:
    - 200: with the following payload:

          {
            "taskName": "task-1",
            "averageTime": 157
          }

    - 404: Task Not found

          {
            "status": 404,
            "messages": [
              "Not Found Task Name: aaa-1"
            ]
          }

    - 500: Internal Server Error        

### Average Processor Microservice
- Get Average Time by Task Name (used internally for api):

- Returns:

  - 200: with the following payload:

        curl -i -H 'Accept: application/json' \
        -H 'Content-Type: application/json' \
        -X GET http://localhost:4013/averages/task-1

   Returns the following payload:
        {
          "taskName": "task-1",
          "averageTime": 157
        }

  - 404: Average Not Found, this payload has more details because should be used only for api:

        {
          "status": 404,
          "messages": [
            "Not Found Task Name: task-1"
          ],
          "uuid": "7f3b4fc2-853b-4120-b609-978b7e400a6e",
          "resourceService": "AVERAGE_PROCESSOR"
        }

    - 500: Internal Server Error   

## Technologic Stack
- Spring Boot
- Spring Cloud Stream
- Spring Data
- MySql
- RabbitMq
- Redis
- Mockito
- jUnit
- Gradle

## Running Services
1. Install, RabbitMq, Redis & MySql. After that modify the three application information (one for each proyect).  

        /src/main/resources/application.yml

2. Go to every project root folder and run:

        gradle build

  and then

       gradle bootRun
