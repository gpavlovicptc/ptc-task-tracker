package com.ptc.task.tracker.store.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Store Processor Microservice Main Class.
 * 
 * 
 */
@SpringBootApplication
@ComponentScan
public class StoreProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreProcessorApplication.class);

	}

}
