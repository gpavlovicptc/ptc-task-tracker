package com.ptc.task.tracker.store.processor.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

/**
 * Config the Stream where to listen (Sink) or push the events.
 * 
 * 
 */
@Configuration
@EnableBinding({EventsSourceConfig.EventsSource.class, EventsSourceConfig.EventsSink.class})
public class EventsSourceConfig {
	public interface EventsSource {
		String AVERAGE_EVENTS = "average-events-source";
		
		@Output(AVERAGE_EVENTS)
		MessageChannel averageEvents();
		
	}

	public interface EventsSink {
		String TASK_EVENTS = "task-events-sink";

		@Input(TASK_EVENTS)
		SubscribableChannel taskEvents();
	}

}
