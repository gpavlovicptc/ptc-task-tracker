package com.ptc.task.tracker.store.processor.service.dto;

/**
 * The Task Information Representation.
 * 
 * 
 */
public class TaskDto {

	private String taskName;

	private Long taskDuration;

	public TaskDto(){
	}

	public TaskDto(String taskName, Long taskDuration){
		this.taskName = taskName;
		this.taskDuration = taskDuration;
	}
	
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

	public Long getTaskDuration() {
		return taskDuration;
	}

	public void setTaskDuration(Long taskDuration) {
		this.taskDuration = taskDuration;
	}

}
