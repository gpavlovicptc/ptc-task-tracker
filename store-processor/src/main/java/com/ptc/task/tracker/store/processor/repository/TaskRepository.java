package com.ptc.task.tracker.store.processor.repository;

import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;

/**
 * Task Repository.
 * 
 * 
 */
public interface TaskRepository {

	/**
	 * Save task.
	 * 
	 * @param taskEntity the task entity to be persist.
	 */
	void saveTask(TaskEntity taskEntity);
	
}
