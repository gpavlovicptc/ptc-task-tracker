package com.ptc.task.tracker.store.processor.service.event;

/**
 * Event Producer Interface.
 * 
 * 
 */
public interface EventProducer {

	/**
	 * Sends a {@link AverageEvent} to the Steam.
	 * @param event the event to be sent
	 */
	void send(AverageEvent event);
	
}
