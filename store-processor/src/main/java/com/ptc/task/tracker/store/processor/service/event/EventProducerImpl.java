package com.ptc.task.tracker.store.processor.service.event;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.support.GenericMessage;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.store.processor.config.EventsSourceConfig.EventsSource;

/**
 * Event Producer Implementation.
 * 
 * 
 */
@Service
public class EventProducerImpl implements EventProducer {

	@Autowired
	private EventsSource source;
	
	@Override
	public void send(AverageEvent event) {
		source.averageEvents().send(new GenericMessage<>(event));
	}

}
