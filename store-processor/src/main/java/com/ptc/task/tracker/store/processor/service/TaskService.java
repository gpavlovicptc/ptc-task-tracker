package com.ptc.task.tracker.store.processor.service;

import com.ptc.task.tracker.store.processor.service.dto.TaskDto;

/**
 * Task Service  Interface.
 * 
 * 
 */
public interface TaskService {

	/**
	 * Saves Task Information.
	 * 
	 * @param taskDto
	 */
	void saveTask(TaskDto taskDto);
	
}
