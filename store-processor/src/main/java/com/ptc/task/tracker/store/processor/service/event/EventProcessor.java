package com.ptc.task.tracker.store.processor.service.event;

/**
 * Event Processor Interface.
 * 
 * 
 */
public interface EventProcessor {

	/**
	 * Process a {@link TaskEvent}.
	 * 
	 * @param event the event
	 */
	void process(TaskEvent event);
}
