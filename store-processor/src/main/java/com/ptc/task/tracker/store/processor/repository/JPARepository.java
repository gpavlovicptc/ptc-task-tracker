package com.ptc.task.tracker.store.processor.repository;

import org.springframework.data.repository.CrudRepository;

import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;

/**
 * Spring Data Interface to do Queries to the Database.
 * 
 * 
 */
public interface JPARepository extends CrudRepository<TaskEntity, Long>  {

}
