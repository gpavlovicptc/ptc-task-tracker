package com.ptc.task.tracker.store.processor.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.store.processor.config.EventsSourceConfig.EventsSink;
import com.ptc.task.tracker.store.processor.service.TaskService;

/**
 * Event Processor Implementation.
 * 
 * 
 */
@Service
public class EventProcessorImpl implements EventProcessor {

	private static final Logger LOG = LoggerFactory.getLogger(EventProcessorImpl.class);
	
	@Autowired
	private TaskService taskService;

	@Override
	@StreamListener(EventsSink.TASK_EVENTS)
	public void process(TaskEvent event) {
		taskService.saveTask(event.getTask());
	
		LOG.info("Processing Task Event. Task name: " + event.getTask().getTaskName() + ", Task Duration: "
				+ event.getTask().getTaskDuration());
	}

}
