package com.ptc.task.tracker.store.processor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.store.processor.repository.TaskRepository;
import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;
import com.ptc.task.tracker.store.processor.service.dto.TaskDto;
import com.ptc.task.tracker.store.processor.service.event.AverageEvent;
import com.ptc.task.tracker.store.processor.service.event.EventProducer;

/**
 * Task Service Implementation.
 * 
 * 
 */
@Service
public class TaskServiceImpl implements TaskService {

	@Autowired
	private TaskRepository taskRepository;

	@Autowired
	private EventProducer eventProducer;

	@Override
	public void saveTask(TaskDto taskDto) {

		TaskEntity taskEntity = createTaskEntity(taskDto);
		taskRepository.saveTask(taskEntity);

		AverageEvent event = new AverageEvent(taskEntity.getTaskName());
		eventProducer.send(event);
	}

	/**
	 * Creates {@link TaskEntity} from {@link TaskDto}
	 * @param taskDto the task dto
	 * @return the task entity
	 */
	private TaskEntity createTaskEntity(TaskDto taskDto) {
		TaskEntity taskEntity = new TaskEntity();
		taskEntity.setTaskName(taskDto.getTaskName());
		taskEntity.setTaskDuration(taskDto.getTaskDuration());
		return taskEntity;
	}

}
