package com.ptc.task.tracker.store.processor.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;

/**
 * Task Repository Implementation.
 * 
 * 
 */
@Repository
public class TaskRepositoryImpl implements TaskRepository {

	@Autowired
	private JPARepository jpaRepository;
	
	@Override
	public void saveTask(TaskEntity taskEntity) {
		jpaRepository.save(taskEntity);
	}

}
