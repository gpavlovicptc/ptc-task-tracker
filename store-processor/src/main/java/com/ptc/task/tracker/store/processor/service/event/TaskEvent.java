package com.ptc.task.tracker.store.processor.service.event;

import java.io.Serializable;

import com.ptc.task.tracker.store.processor.service.dto.TaskDto;

/**
 * Task Event Representation.
 * 
 * 
 */
public class TaskEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	private static String kind;

	private TaskDto task;

	public TaskEvent (){
	}
	
	public TaskEvent (TaskDto task){
		this.task = task;
	}
	
	public static String getKind() {
		return kind;
	}

	public static void setKind(String kind) {
		TaskEvent.kind = kind;
	}

	public void setTask(TaskDto task) {
		this.task = task;
	}
	public TaskDto getTask() {
		return task;
	}
}
