package com.ptc.task.tracker.store.processor.repository;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;
import static org.mockito.Mockito.verify;

/**
 * Task Repository Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TaskRepositoryImplTest {

	@Mock
	private JPARepository jpaRepositoryMock;
	
	@InjectMocks
	private TaskRepositoryImpl taskRepository;
	
	@Test
	public void saveTaskTest(){
		
		TaskEntity taskEntity = new TaskEntity();
		
		taskRepository.saveTask(taskEntity);
		
		verify(jpaRepositoryMock).save(taskEntity);
	}
	
}
