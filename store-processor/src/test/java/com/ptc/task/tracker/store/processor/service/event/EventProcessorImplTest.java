package com.ptc.task.tracker.store.processor.service.event;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.store.processor.service.TaskService;
import com.ptc.task.tracker.store.processor.service.dto.TaskDto;

/**
 * Event Processor Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class EventProcessorImplTest {

	private static final String TASK_NAME = "task-20";
	private static final Long TASK_DURATION = 10L;
	
	@Mock
	private TaskService taskServiceMock;
	
	@InjectMocks
	private EventProcessorImpl eventProcessor;
	
	@Test
	public void processEventTest(){
		TaskDto taskDto = new TaskDto(TASK_NAME, TASK_DURATION);
		TaskEvent event = new TaskEvent();
		event.setTask(taskDto);
	
		eventProcessor.process(event);
		
		verify(taskServiceMock).saveTask(taskDto);
	}
}
