package com.ptc.task.tracker.store.processor.service.event;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.messaging.MessageChannel;

import com.ptc.task.tracker.store.processor.config.EventsSourceConfig.EventsSource;

/**
 * Event Producer Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class EventProducerImplTest {

	private static final String TASK_NAME = "task-20";

	@Mock
	private EventsSource sourceMock;

	@InjectMocks
	private EventProducerImpl eventProducer;
	
	@Test
	public void sendEventTest() {

		AverageEvent averageEvent = new AverageEvent();
		averageEvent.setTaskName(TASK_NAME);
		
		MessageChannel channel = mock(MessageChannel.class);

		when(sourceMock.averageEvents()).thenReturn(channel);

		eventProducer.send(averageEvent);

		verify(sourceMock).averageEvents();

	}
	
}
