package com.ptc.task.tracker.store.processor.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.store.processor.repository.TaskRepository;
import com.ptc.task.tracker.store.processor.repository.entity.TaskEntity;
import com.ptc.task.tracker.store.processor.service.TaskServiceImpl;
import com.ptc.task.tracker.store.processor.service.dto.TaskDto;
import com.ptc.task.tracker.store.processor.service.event.AverageEvent;
import com.ptc.task.tracker.store.processor.service.event.EventProducer;

import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;

/**
 * Event Task Service Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class TaskServiceImplTest {
	
	private static final String TASK_NAME = "task-20";
	
	private static final Long TASK_DURATION = 10L;
	
	@Mock
	private TaskRepository taskRepositoryMock;

	@Mock
	private EventProducer eventProducerMock;
	
	@InjectMocks
	private TaskServiceImpl traskService;
	
	@Test
	public void saveTaskTest(){
		TaskDto taskDto = new TaskDto(TASK_NAME, TASK_DURATION);

		traskService.saveTask(taskDto);
		
		verify(taskRepositoryMock).saveTask(any(TaskEntity.class));
		verify(eventProducerMock).send(any(AverageEvent.class));

	}
	
}
