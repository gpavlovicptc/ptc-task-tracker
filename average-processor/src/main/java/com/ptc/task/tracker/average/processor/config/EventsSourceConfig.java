package com.ptc.task.tracker.average.processor.config;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.SubscribableChannel;

/**
 * Config the Stream where to listen the events.
 * 
 * 
 */
@Configuration
@EnableBinding({EventsSourceConfig.EventsSink.class})
public class EventsSourceConfig {

	public interface EventsSink {
		
		String AVERAGE_EVENTS = "average-events-sink";

		@Input(AVERAGE_EVENTS)
		SubscribableChannel taskEvents();
	}

}
