package com.ptc.task.tracker.average.processor.repository;

/**
 * Average Repository Interface.
 * 
 * 
 */
public interface AverageRepository {

	/**
	 * Gets the average by task Name.
	 * 
	 * @param taskName
	 * @return the average
	 */
	long getAverage(String taskName);
	
}
