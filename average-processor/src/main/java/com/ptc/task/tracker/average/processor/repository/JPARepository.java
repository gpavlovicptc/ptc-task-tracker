package com.ptc.task.tracker.average.processor.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.ptc.task.tracker.average.processor.repository.entity.TaskEntity;

/**
 * Spring Data Interface to do Queries to the Database.
 * 
 * 
 */
public interface JPARepository extends CrudRepository<TaskEntity, Long>  {

	/**
	 * Get Average Duration by Task Name.
	 * 
	 * @param taskName the task name
	 * @return the average
	 */
	@Query("SELECT SUM(t.taskDuration)/COUNT(t) FROM TaskEntity t WHERE t.taskName=:taskName")
	public Long getAverageByTaskName(@Param("taskName") String taskName);	
}
