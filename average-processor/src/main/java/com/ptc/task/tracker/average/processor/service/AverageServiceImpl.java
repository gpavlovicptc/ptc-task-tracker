package com.ptc.task.tracker.average.processor.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.average.processor.repository.AverageRepository;
import com.ptc.task.tracker.average.processor.repository.CacheRepository;

/**
 * Average Service Implementation
 * 
 * 
 */
@Service
public class AverageServiceImpl implements AverageService {

	@Autowired
	private CacheRepository cacheRepository;

	@Autowired
	private AverageRepository taskRepository;

	@Override
	public void calculateAverage(String taskName) {
		Long average = taskRepository.getAverage(taskName);
		cacheRepository.addAverageByTaskName(taskName, average);
	}

	@Override
	public long getAverage(String taskName) {
		return cacheRepository.getAverageByTaskName(taskName);
	}
}
