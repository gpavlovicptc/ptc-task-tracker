package com.ptc.task.tracker.average.processor.service.event;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import com.ptc.task.tracker.average.processor.config.EventsSourceConfig.EventsSink;
import com.ptc.task.tracker.average.processor.service.AverageService;

/**
 * Event Processor Implementation.
 * 
 * 
 */
@Service
public class EventProcessorImpl implements EventProcessor {
	
	private static final Logger LOG = LoggerFactory.getLogger(EventProcessorImpl.class);
	
	@Autowired
	private AverageService averageService;

	@Override
	@StreamListener(EventsSink.AVERAGE_EVENTS)
	public void process(AverageEvent event) {
		averageService.calculateAverage(event.getTaskName());
		
		LOG.info("Processing Average Event. Task name: " + event.getTaskName());
		
	}

}
