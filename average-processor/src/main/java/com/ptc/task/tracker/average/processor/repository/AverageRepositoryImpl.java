package com.ptc.task.tracker.average.processor.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * Average Repository Impl. It uses JPA to query the database.
 * 
 * 
 */
@Repository
public class AverageRepositoryImpl implements AverageRepository {

	@Autowired
	private JPARepository jpaRepository;

	@Override
	public long getAverage(String taskName) {
		Long average = jpaRepository.getAverageByTaskName(taskName);
		return average == null ? 0 : average;
	}
	
}
