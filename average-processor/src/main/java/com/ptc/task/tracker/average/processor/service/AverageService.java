package com.ptc.task.tracker.average.processor.service;

/**
 * Avergae Service.
 * 
 * 
 */
public interface AverageService {

	/**
	 * Calculates the average bt task name.
	 * 
	 * @param taskName the task name.
	 */
	void calculateAverage(String taskName);
	
	/**
	 * Gets the average by task name.
	 * 
	 * @param taskName the task name
	 * @return the average
	 */
	long getAverage(String taskName);
}
