package com.ptc.task.tracker.average.processor.exception;

import java.util.Set;

/**
 * Response REST Error.
 * 
 * 
 */
public class ResponseRestError {

  private int status;

  private Set<String> messages;

  private String uuid;

  private String resourceService;

  public int getStatus() {
    return status;
  }

  public Set<String> getMessages() {
    return messages;
  }

  public String getUuid() {
    return uuid;
  }

  public String getResourceService() {
    return resourceService;
  }

  public static Builder builder() {
    return new Builder();
  }

  public static class Builder {
    private ResponseRestError apiError;

    private Builder() {
      this.apiError = new ResponseRestError();
    }

    public Builder status(int status) {
      this.apiError.status = status;
      return this;
    }

    public Builder message(Set<String> messages) {
      this.apiError.messages = messages;
      return this;
    }

    public Builder uuid(String uuid) {
      this.apiError.uuid = uuid;
      return this;
    }

    public Builder resourceService(String resourceService) {
      this.apiError.resourceService = resourceService;
      return this;
    }

    public ResponseRestError build() {
      return apiError;
    }
  }
}
