package com.ptc.task.tracker.average.processor.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import com.ptc.task.tracker.average.processor.exception.AverageNotFoundException;

/**
 * Implementation of {@link CacheRepository}.
 * It uses redis to persist and return the average. 
 * The key used is taskName_{$name}.
 * 
 * 
 */
@Repository
public class RedisRepository implements CacheRepository {

	private static final String KEY_FORMAT = "taskName_%s";

	@Autowired
	private RedisTemplate<String, String> redisTemplate;

	@Override
	public long getAverageByTaskName(String taskName) {
		String key = generateRedisKey(taskName);
		
		String averageFromRedis = redisTemplate.opsForValue().get(key);
		
		if (averageFromRedis == null){
			throw new AverageNotFoundException("Not Found Task Name: " + taskName);
		}
		
		return Long.valueOf(averageFromRedis);
	}

	@Override
	public void addAverageByTaskName(String taskName, long average) {
		String key = generateRedisKey(taskName);
		redisTemplate.opsForValue().set(key, String.valueOf(average));
	}

	/**
	 * Generates the key to persist or return the information.
	 * @param taskName the task name.
	 * @return the key
	 */
	private String generateRedisKey(String taskName) {
		return String.format(KEY_FORMAT, taskName);
	}
}
