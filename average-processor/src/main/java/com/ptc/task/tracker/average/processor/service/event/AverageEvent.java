package com.ptc.task.tracker.average.processor.service.event;

import java.io.Serializable;

/**
 * Task Event to be listen from the Stream.
 * 
 * 
 */
public class AverageEvent implements Serializable {

	private static final long serialVersionUID = 1L;

	private String taskName;

	public AverageEvent(){
	}
	
	public AverageEvent(String taskName){
		this.taskName = taskName;
	}
	
	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

}
