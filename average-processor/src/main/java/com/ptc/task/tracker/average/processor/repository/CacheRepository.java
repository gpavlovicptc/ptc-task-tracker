package com.ptc.task.tracker.average.processor.repository;

/**
 * Cache Repository Interface.
 * 
 * 
 */
public interface CacheRepository {

	/**
	 * Adds average by Task Name.
	 * 
	 * @param taskName the task name
	 * @param average the average
	 */
	void addAverageByTaskName(String taskName, long average);
	
	/**
	 * Gets the average by Task Name.
	 * 
	 * @param taskName the task name
	 * @return the average
	 */
	long getAverageByTaskName(String taskName);
	
}
