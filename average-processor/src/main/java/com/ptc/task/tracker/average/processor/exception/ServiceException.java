package com.ptc.task.tracker.average.processor.exception;

import org.springframework.http.HttpStatus;

/**
 * Service Exception. Class to extends Custom Exceptions.
 * 
 * 
 */
public abstract class ServiceException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	private HttpStatus statusCode;

	public ServiceException(String message, HttpStatus statusCode) {
		super(message);
		this.statusCode = statusCode;
	}

	public HttpStatus getStatusCode() {
		return statusCode;
	}

}
