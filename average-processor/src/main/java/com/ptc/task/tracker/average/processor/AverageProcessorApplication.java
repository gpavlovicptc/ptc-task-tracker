package com.ptc.task.tracker.average.processor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Average Processor Microservice Main Class.
 * 
 * 
 */
@SpringBootApplication
@ComponentScan
public class AverageProcessorApplication {

	public static void main(String[] args) {
		SpringApplication.run(AverageProcessorApplication.class);
	}

}
