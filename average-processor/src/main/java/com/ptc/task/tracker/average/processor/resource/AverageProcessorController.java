package com.ptc.task.tracker.average.processor.resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ptc.task.tracker.average.processor.resource.dto.TaskDto;
import com.ptc.task.tracker.average.processor.service.AverageService;

/**
 * Task Dto. The task information used in
 * the main controller and services.
 * 
 * 
 */
@RestController
@RequestMapping(value = "/averages")
public class AverageProcessorController {

	@Autowired
	private AverageService averageService;

	/**
	 * Gets average by task name. Or 404 if the resource is not found it.
	 * 
	 * @param taskName the task name
	 * @return TaskDto the Task Information
	 */
	@RequestMapping(value = "/{taskName}", method = RequestMethod.GET, 
			produces = MediaType.APPLICATION_JSON_VALUE)
	public TaskDto getAverage( @PathVariable("taskName") String taskName) {
		
		long average = averageService.getAverage(taskName);
		
		TaskDto response = new TaskDto();
		response.setTaskName(taskName);
		response.setAverageTime(average);
		
		return response;
	}

}
