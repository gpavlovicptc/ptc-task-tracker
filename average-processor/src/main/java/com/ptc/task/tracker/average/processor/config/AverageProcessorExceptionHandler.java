package com.ptc.task.tracker.average.processor.config;

import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.ptc.task.tracker.average.processor.exception.ResponseRestError;
import com.ptc.task.tracker.average.processor.exception.ServiceException;

/**
 * AverageProcessorExceptionHandler handles 
 * the errors in Average Processor Microservice.
 * 
 * 
 */
@ControllerAdvice
public class AverageProcessorExceptionHandler extends ResponseEntityExceptionHandler {

	private static final Logger LOG = LoggerFactory.getLogger(AverageProcessorExceptionHandler.class);

	private static final String ERROR_UUID_SUFFIX = " - Error uuid: ";

	private static final String RESOURCE_SERVICE_NAME = "AVERAGE_PROCESSOR";

	/**
	 * Handle the Default Exceptions.
	 * 
	 * @param ex the exception thrown
	 * @param the current request
	 * @return the custom response
	 */
	@ExceptionHandler({ Exception.class })
	public ResponseEntity<Object> handleDefaultExceptions(final Exception ex, final WebRequest request) {
		Set<String> errorMessages = new HashSet<>();
		errorMessages.add(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase());

		return getExceptionResponse(HttpStatus.INTERNAL_SERVER_ERROR, errorMessages, ex);
	}
	
	/**
	 * Handle the {@link ServiceException}.
	 * 
	 * @param ex the exception thrown
	 * @param the current request
	 * @return the custom response
	 */
	@ExceptionHandler({ ServiceException.class })
	private ResponseEntity<Object> getServiceExceptionResponse(ServiceException ex) {
		Set<String> errorMessages = new HashSet<>();
		errorMessages.add(ex.getMessage());

		return getExceptionResponse(ex.getStatusCode(), errorMessages, ex);
	}

	/**
	 * Build the Custom Error Response.
	 * 
	 * @param statusCode the {@link HttpStatus} to be throw
	 * @param messages the messages to show
	 * @param ex the exception 
	 * @return custom REST response.
	 */
	private ResponseEntity<Object> getExceptionResponse(HttpStatus statusCode, Set<String> messages, Exception ex) {
		String uuid = UUID.randomUUID().toString();

		ResponseRestError apiError = ResponseRestError.builder().status(statusCode.value()).message(messages)
				.resourceService(RESOURCE_SERVICE_NAME).uuid(uuid).build();

		LOG.error(ERROR_UUID_SUFFIX + uuid, ex);

		return new ResponseEntity<>(apiError, new HttpHeaders(), statusCode);
	}
}
