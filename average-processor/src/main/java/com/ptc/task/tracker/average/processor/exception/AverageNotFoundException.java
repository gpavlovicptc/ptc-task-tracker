package com.ptc.task.tracker.average.processor.exception;

import org.springframework.http.HttpStatus;

/**
 * Average Not Found Exception Class 
 * 
 * 
 */
public class AverageNotFoundException extends ServiceException{

	private static final long serialVersionUID = 1L;

	private static HttpStatus statusCode = HttpStatus.NOT_FOUND;
	
	public AverageNotFoundException(String message) {
		super(message, statusCode);
	}

}
