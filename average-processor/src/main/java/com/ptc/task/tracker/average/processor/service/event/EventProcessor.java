package com.ptc.task.tracker.average.processor.service.event;

/**
 * Event Processor Interface.
 * 
 * 
 */
public interface EventProcessor {

	/**
	 * Process a {@link AverageEvent} 
	 * @param event the event
	 */
	void process(AverageEvent event);
}
