package com.ptc.task.tracker.average.processor.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;

import redis.clients.jedis.JedisPoolConfig;

/**
 * Redis Configuration.
 * 
 * 
 */
@Configuration
public class RedisConfiguration {

	private @Value("${ptc.average.processor.redis.hostName}") String redisHostName;
	private @Value("${ptc.average.processor.redis.port}") int redisPort;
	private @Value("${ptc.average.processor.redis.poolConfig.maxTotal}") int redisMaxTotal;
	private @Value("${ptc.average.processor.redis.poolConfig.maxIdle}") int redisMaxIdle;
	private @Value("${ptc.average.processor.redis.poolConfig.minIdle}") int redisMinIdle;
	private @Value("${ptc.average.processor.redis.poolConfig.testOnBorrow}") boolean redisTestOnBorrow;
	private @Value("${ptc.average.processor.redis.poolConfig.testOnReturn}") boolean redisTestOnReturn;
	private @Value("${ptc.average.processor.redis.poolConfig.testWhileIdle}") boolean redisTestWhileIdle;

	/**
	 * Config Jedis Connection Factory.
	 * 
	 * @return JedisConnectionFactory
	 */
	@Bean
	public JedisConnectionFactory jedisConnectionFactory() {
		JedisPoolConfig poolConfig = new JedisPoolConfig();
		poolConfig.setMaxTotal(redisMaxTotal);
		poolConfig.setMaxIdle(redisMaxIdle);
		poolConfig.setMinIdle(redisMinIdle);
		poolConfig.setTestOnBorrow(redisTestOnBorrow);
		poolConfig.setTestOnReturn(redisTestOnReturn);
		poolConfig.setTestWhileIdle(redisTestWhileIdle);

		JedisConnectionFactory factory = new JedisConnectionFactory(poolConfig);
		factory.setHostName(redisHostName);
		factory.setPort(redisPort);
		factory.setUsePool(true);
		return factory;
	}

	/**
	 * Redis Template Bean Configuration.
	 * 
	 * @return the RedisTemplate
	 */
	@Bean
	public RedisTemplate<String, Object> redisTemplate() {
		final RedisTemplate<String, Object> template = new RedisTemplate<String, Object>();
		template.setConnectionFactory(jedisConnectionFactory());
		return template;
	}

}
