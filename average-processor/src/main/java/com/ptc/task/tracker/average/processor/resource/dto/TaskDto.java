package com.ptc.task.tracker.average.processor.resource.dto;

/**
 * Task Dto. The task information used in
 * the main controller and services.
 * 
 * 
 */
public class TaskDto {

	private String taskName;

	private Long averageTime;
	
	public Long getAverageTime() {
		return averageTime;
	}

	public void setAverageTime(Long averageTime) {
		this.averageTime = averageTime;
	}

	public String getTaskName() {
		return taskName;
	}

	public void setTaskName(String taskName) {
		this.taskName = taskName;
	}

}
