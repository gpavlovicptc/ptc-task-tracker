package com.ptc.task.tracker.average.processor.service;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.average.processor.repository.AverageRepository;
import com.ptc.task.tracker.average.processor.repository.CacheRepository;

/**
 * Average Service Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class AverageServiceImplTest {
	
	private static final String TASK_NAME = "task-20";

	private static final long AVERAGE = 1L;

	@Mock
	private CacheRepository cacheRepositoryMock;

	@Mock
	private AverageRepository averageRepository;

	@InjectMocks
	private AverageServiceImpl averageService;

	@Test
	public void calculateAverageTest() {

		when(averageRepository.getAverage(TASK_NAME)).thenReturn(AVERAGE);
		
		averageService.calculateAverage(TASK_NAME);

		verify(averageRepository).getAverage(TASK_NAME);
		
		verify(cacheRepositoryMock).addAverageByTaskName(TASK_NAME, AVERAGE);

	}
	
	@Test
	public void getAverage(){
		when(cacheRepositoryMock.getAverageByTaskName(TASK_NAME)).thenReturn(AVERAGE);
		
		averageService.getAverage(TASK_NAME);
		
		verify(cacheRepositoryMock).getAverageByTaskName(TASK_NAME);

	}

}
