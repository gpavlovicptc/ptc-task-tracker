package com.ptc.task.tracker.average.processor.service.event;

import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.ptc.task.tracker.average.processor.service.AverageService;

/**
 * Event Processor Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class EvenProcessorImplTest {

	private static final String TASK_NAME = "task-20";
	
	@Mock
	private AverageService averageServiceMock;
	
	@InjectMocks
	private EventProcessorImpl eventProducer;
	
	@Test
	public void processEventTest() {
		AverageEvent event = new AverageEvent();
		event.setTaskName(TASK_NAME);
		
		eventProducer.process(event);

		verify(averageServiceMock).calculateAverage(TASK_NAME);
	}
	
}
