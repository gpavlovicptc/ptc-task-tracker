package com.ptc.task.tracker.average.processor.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;

import com.ptc.task.tracker.average.processor.exception.AverageNotFoundException;

/**
 * Redis Repository Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class RedisRepositoryTest {
	
	private static final String TASK_NAME = "task-20";
	private static final String TASK_KEY = "taskName_task-20";
	private static final String AVERAGE_STRING = "1";
	private static final long AVERAGE_LONG = 1L;
	
	@InjectMocks
	private RedisRepository redisRepository;
	
	@Mock
	private RedisTemplate<String, String> redisTemplateMock;
	
	@Test
	@SuppressWarnings({"unchecked","rawtypes"})
	public void getAverageByTaskNameAndReturnOk(){
		ValueOperations valueOperations = mock(ValueOperations.class);
	    when(redisTemplateMock.opsForValue()).thenReturn(valueOperations);
		when(redisTemplateMock.opsForValue().get(TASK_KEY)).thenReturn(AVERAGE_STRING);
		
		long averageReturned = redisRepository.getAverageByTaskName(TASK_NAME);
		
		assertEquals(AVERAGE_LONG, averageReturned);	
	}

	@Test(expected = AverageNotFoundException.class)
	@SuppressWarnings({"unchecked","rawtypes"})
	public void getAverageByTaskNameAndReturnExceptionTest(){
		ValueOperations valueOperations = mock(ValueOperations.class);
	    when(redisTemplateMock.opsForValue()).thenReturn(valueOperations);
		when(redisTemplateMock.opsForValue().get(TASK_KEY)).thenReturn(null);
		
		redisRepository.getAverageByTaskName(TASK_NAME);
		
		verify(redisTemplateMock).opsForValue();	
	}
	
	@Test
	@SuppressWarnings({"unchecked","rawtypes"})
	public void addAverageByTaskNameTest(){
		ValueOperations valueOperations = mock(ValueOperations.class);
	    when(redisTemplateMock.opsForValue()).thenReturn(valueOperations);
		
		redisRepository.addAverageByTaskName(AVERAGE_STRING, AVERAGE_LONG);
		
		verify(redisTemplateMock).opsForValue();
		
	}
	
}
