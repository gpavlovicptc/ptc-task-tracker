package com.ptc.task.tracker.average.processor.repository;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

/**
 * Average Repository Implementation Test.
 * 
 * 
 */
@RunWith(MockitoJUnitRunner.class)
public class AverageRepositoryImplTest {

	private static final String TASK_NAME = "task-20";

	private static final long AVERAGE_LONG = 1L;

	private static final long AVERAGE_DEFAULT = 0L;

	@Mock
	private JPARepository jpaRepositoryMock;

	@InjectMocks
	private AverageRepositoryImpl averageRepository;

	@Test
	public void getAverageForNullTaskAndReturnDefaultAvgTest() {

		when(jpaRepositoryMock.getAverageByTaskName(TASK_NAME)).thenReturn(null);

		long averageReturned = averageRepository.getAverage(TASK_NAME);

		assertEquals(AVERAGE_DEFAULT, averageReturned);
	}

	@Test
	public void getAverageForTaskAndReturnAvgTest() {

		when(jpaRepositoryMock.getAverageByTaskName(TASK_NAME)).thenReturn(AVERAGE_LONG);

		long averageReturned = averageRepository.getAverage(TASK_NAME);

		assertEquals(AVERAGE_LONG, averageReturned);
	}

}
